package de.thm.arsnova.service.httpgateway.model

import java.util.Date

data class AnnouncementState(val total: Int, val new: Int, val readTimestamp: Date?)
