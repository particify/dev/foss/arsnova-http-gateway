package de.thm.arsnova.service.httpgateway.model

data class RoomRole(
    val userId: String,
    val role: String
)
