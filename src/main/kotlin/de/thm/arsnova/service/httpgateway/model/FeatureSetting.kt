package de.thm.arsnova.service.httpgateway.model

data class FeatureSetting(
    var tierId: String = "",
    var key: String = "",
    var value: String = ""
)
