package de.thm.arsnova.service.httpgateway.model

enum class AccessLevel {
    CREATOR,
    EXECUTIVE_MODERATOR,
    PARTICIPANT,
    NONE
}
