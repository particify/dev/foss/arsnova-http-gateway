package de.thm.arsnova.service.httpgateway.model

enum class AccessChangeRequestType {
    CREATE,
    DELETE,
    DELETE_ALL
}
